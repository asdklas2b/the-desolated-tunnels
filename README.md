# The Desolated Tunnels

## Installatie

### Stap 1: Submodules

Voor het gebruik van de submodules moet je ssh via [git](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/) geconfigureerd hebben. 

Nadat git via ssh geconfigureerd is kan je met het volgende commando de submodules initializeren:

```
git submodule update --init
```

### Stap 2: Java & Gradle

Voor het bouwen van de applicatie is java 15 nodig.

Nadat deze geinstalleerd is kan je met het volgende commando kijken of gradle op de juiste manier is geconfigureerd:

```
# Unix
bash ./gradlew clean
# Windows
gradlew.bat clean
```

### Stap 3: Testen & Bouwen

Door het uitvoeren van het volgende commando test en bouwt gradle de applicatie.

```
# Unix 
bash ./gradlew build
# Windows
gradlew.bat build
```

### Stap 4: Uitvoeren

```
java -jar app/build/libs/TheDesolatedTunnels.jar
```



